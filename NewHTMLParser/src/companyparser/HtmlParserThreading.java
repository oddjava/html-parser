package companyparser;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/* This code is used for parsing Html documents downloaded from linkedIn. 
 * This code extract 27 fields from Html doc and adds to db.
 * This is multithreaded code and process batch of 50 documents.
 * This code updates sellerStatus as either true, invalidDomain, parentDomain, 
   linkedInDomain or blankDomain. 
 * This code updates parsingStatus as either parsed or duplicate. By default, false. 
 * */

public class HtmlParserThreading 
{
	// Variables to display parsing Report after completion of code
	static String startTime;
	static String endTime;
	public static int totalInput=0; 
	public static int totalScraped=0;
	public static int totalParsed=0;
	public static int parentDomain=0;
	public static int blankDomain=0;
	public static int notFoundDomain=0;
	public static int linkedINDomain=0;	
	public static int duplicate=0;
	
	// Refernces related to Mongo Databases
	MongoClient mongoClient;
	DB db;
	DBCollection htmlDoc;
	DBCollection parsedCollection;
	DBCollection geodata;
	
	// References related to MultiThreading
	static ExecutorService executor;
	static Runnable worker;
	
	// Constructor to establish DB connection and to select collection.
	HtmlParserThreading(String ip)
	{
		mongoClient = new MongoClient(ip, 27017);
		db = mongoClient.getDB("sampleParser");
		htmlDoc = db.getCollection("htmlDocument");
		parsedCollection=db.getCollection("parsedData");
		geodata = db.getCollection("geoLocation");		
	}
	
	/* This function take documents from htmlDocument collection 
	   which are in given id range and whose parsingStatus is false. */
	boolean parseHTML(int startid,int endid)
	{		
		DBCursor cursor=null;
		
		cursor = htmlDoc.find(new BasicDBObject("_id", new BasicDBObject("$lte", endid).append("$gte", startid)).append("parsingStatus", "false"));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		System.out.println("\nTotal records selected for parsing = "+cursor.count()+"\n\n");
					
		ArrayList<DBObject> object=new ArrayList<DBObject>();
		
		while (cursor.hasNext()) 
		{			
			object.add(cursor.next());
			
			if (object.size() == 50)  //Check whether batch of 50 is ready.
			{	
				callThread(object);   // send those 50 doc for parsing
				object.clear();		  // Clear list of 50 docs.
				System.gc();		  // call garbage collector
			}
		}	
		
		if(object.size()>0)  // for batch of remaining doc <50
		{
			callThread(object);	// send those 50 doc for parsing
			object.clear();		// Clear list of remaining docs.
			System.gc();		// call garbage collector
		}
	
		cursor.close(); //close cursor after every thing is done with current cursor
		return true;
	}
	
	/* This method take batch of documents and submit each document to 
	 * ThreadPool of 10 threads. 10 threads will process batch of docs.*/
	void callThread(ArrayList<DBObject> object)
	{
		String link;
		String doc;
		String id;	
		System.out.println("\n\n\t\t\t***************** Started Batch of "+object.size()+" ****************");
		executor = Executors.newFixedThreadPool(10); 
		for(int i=0;i<object.size();i++)
			{
				link = "";
				doc = "";
				id = "";
				link= object.get(i).get("link").toString();
				doc = object.get(i).get("doc").toString();
				id  = object.get(i).get("_id").toString();
				//Give id, link and doc to each thread. 
				worker = new MyRunnable(htmlDoc,geodata,parsedCollection,id,link,doc); 
				executor.execute(worker);  //submit each doc to thread pool.
			}
			executor.shutdown(); //shutdown all threads after completion.
			
			// Wait until all threads are finish
			while (!executor.isTerminated()) 
			{	}
			
		System.out.println("\n\n\t\t\t***************** Finished Batch of "+object.size()+" ****************");
	}
	
	// This method will  display time taken and parsing report. 		
	public static void showCount()
	{
		endTime=new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date());
		totalScraped=blankDomain+parentDomain+notFoundDomain+linkedINDomain;

		System.out.println("\n\n\n*************** Parsing Report *********************\n");
		System.out.println("Total Input     = "+totalInput);
		System.out.println("\nTotal Valid     = "+totalParsed);
		System.out.println("Total Scraped   = "+totalScraped);
		System.out.println("\n\nTotal Parent    = "+parentDomain);
		System.out.println("Total Blank     = "+blankDomain);
		System.out.println("Total Invalid   = "+notFoundDomain);
		System.out.println("Total LinkedIN  = "+linkedINDomain);
		System.out.println("\n\nTotal Duplicate = "+duplicate);		
		
		System.out.println("\n\nStart Time = "+startTime);
		System.out.println("End Time   = "+endTime);

		System.out.println("\n\n\n\n");
	}
		
	// This method takes input from user. host and id range.
	public static void main(String[] args) 
	{
		String ip=null;
		int startid,endid;
		Scanner sc=null;

		sc = new Scanner(System.in);
		System.out.println("Enter ip address");
		ip= sc.nextLine();
		System.out.println("Enter start id ");
		startid = sc.nextInt();
		System.out.println("Enter end id (Multiple of 100)");
		endid = sc.nextInt();
		sc.close();

		HtmlParserThreading parser=null;

		int diff=endid-startid;
		HtmlParserThreading.totalInput=diff+1;

		int tempEnd=0;

		// Record start time
		startTime=new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date());

		// Create internal slabs of 100 docs from given rage.
		while(diff+1>=100 && startid<=endid)
		{
			tempEnd=startid+99;
			System.out.println("\nIteration startid="+startid+"\t Endid="+tempEnd);
			parser=new HtmlParserThreading(ip);
			parser.parseHTML(startid,tempEnd);
			startid=tempEnd+1;
			diff=diff-99;
			parser.mongoClient.close();
			parser=null;
			System.gc();				
		}
		
		//Display parsing report when everything is done.
		showCount();
	}
}


class MyRunnable implements Runnable 
{
	// Variable to hold htmlDocument collection fields.
	String id=null;
	String link=null;
	String doc=null;
	
	// References to hold DBcollection. 
	DBCollection parsedCollection=null;
	DBCollection geodata=null;
	DBCollection htmlDoc=null;
	
	// Regex to validate domain link.
	static final String DOMAIN_NAME_PATTERN = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,}$";
	
	// Constructor to initialize variables of thread.
	MyRunnable(DBCollection htmlDoc,DBCollection geodata,DBCollection parsedCollection,String id,String link,String doc) 
	{
		this.id = id;
		this.link = link;
		this.doc = doc;
		this.parsedCollection = parsedCollection;
		this.htmlDoc = htmlDoc;
		this.geodata = geodata;
	}

	/* This method will execute independently.
	 * This will scan htmldoc and extracts JSON String of company information.*/ 
	public void run() 
	{				
		// Variable for founded year of company.
		String founded = "0";
		
		// References related to JSON operation
		String jsonfile = "";
		JSONArray specialties=null;
		JSONObject jsonObject = null;
			
		// Following are the four classes(with Hierarchy) to extract JSON String.
		try 
		{
			if((jsonfile = StringUtils.substringBetween(doc, "stream-insights-embed-id-content", "--></code>"))!=null)
			{
				jsonfile = jsonfile.substring(11);
			}
			else if((jsonfile = StringUtils.substringBetween(doc, "stream-promo-top-bar-embed-id-content", "--></code>"))!=null)
			{
				jsonfile = jsonfile.substring(11);				
			}
			else if((jsonfile = StringUtils.substringBetween(doc, "stream-footer-embed-id-content", "--></code>"))!=null)
			{
				jsonfile = jsonfile.substring(11);
			}
			else if((jsonfile = StringUtils.substringBetween(doc, "stream-feed-embed-id-content", "--></code>"))!=null)
			{
				jsonfile = jsonfile.substring(11);
			}				
		} 
		catch (Exception e) //If no class found
		{			
			htmlDoc.update(new BasicDBObject().append("_id", Integer.parseInt(id)), new BasicDBObject().append("$set", new BasicDBObject().append("parsingStatus", "noJSONFound")));
			System.out.println("In Exception. Not Found Any class. Scraped. ID="+id);				
		}
		
		//If JSON string is null or blank => this method is finished.
		if(jsonfile==null || jsonfile.trim().equals(""))
		{
			htmlDoc.update(new BasicDBObject().append("_id", Integer.parseInt(id)), new BasicDBObject().append("$set", new BasicDBObject().append("parsingStatus", "noJSONFound")));
			System.out.println("Not Found Any class. Scraped ID="+id);				
		}	
		else //If JSON String is found => this method will process JSON String.
		{	
			try 
			{ 
				// Map to store field and its value which will get stored in database.
				Map<String,String> bean=new LinkedHashMap<String,String>();
				
				// Pass JSON string to JSON Object.
				jsonObject = new JSONObject(jsonfile);
				
				
	// Title of a company. 
				try 
				{
					bean.put("title",jsonObject.getString("companyName"));
				} catch (Exception e) {	bean.put("title","");	}
				
	// Description is by default empty. Metadata code will update it.
				bean.put("description", "");
				
	// Link and DomainLink of a company.
				String website = "";
				String url="";
				String domainLink="";
				String status="false";
				
				try 
				{
					website = jsonObject.getString("website");
					url=website;
					
					if(!website.trim().equals("")) //If website is found
					{
						try //for getting valid host name
						{
							domainLink=new URL(website).getHost().trim();
							
							// Append www. if not present in domainlink.
							domainLink=(!domainLink.startsWith("www."))? ("www."+domainLink):domainLink;
							
							// Replace http and https for checking of parent Domain. Occurrence of slash (/). 
							website = website.replace("http://", "").replace("https://", "").trim();
							
							if(website.contains("/")) //If / is present in link
							{
								// Take substring from beginning to first occurrence of slash.
								String path=website.substring(website.indexOf("/"),website.length());
								
								if(!path.equals("/")) //Parent Domain. Something is present after /.
								{
									status="parentDomain";
									HtmlParserThreading.parentDomain++;
								}
								else //Valid Domain. Only / present at the end. 
								{									
									//Check whether domain name is valid using Regex.
									if(Pattern.compile(DOMAIN_NAME_PATTERN).matcher(domainLink).find())
									{
										status="true";
										HtmlParserThreading.totalParsed++;
									}
									else //Invalid domain.
									{
										status="invalidDomain";	
										HtmlParserThreading.notFoundDomain++;
									}
								}
							}
							else  //Link without containing /. 
							{
								//Check whether domain name is valid using Regex.
								if(Pattern.compile(DOMAIN_NAME_PATTERN).matcher(domainLink).find())
								{
									status="true";
									HtmlParserThreading.totalParsed++;
								}
								else //Invalid domain
								{
									status="invalidDomain";	
									HtmlParserThreading.notFoundDomain++;
								}
							}
						}
						catch(MalformedURLException e) //Invalid Host by URL class.
						{
							status="invalidDomain";
							HtmlParserThreading.notFoundDomain++;
						}						
					}
					else //Blank Domain.
					{
						status="blankDomain";
						HtmlParserThreading.blankDomain++;
					}					
				} 
				catch (Exception e)  // Blank Domain.
				{
					website = "";
					url="";
					domainLink="";
					status="blankDomain";
					HtmlParserThreading.blankDomain++;
				}				
								
				if(url.contains("linkedin.com"))
				{
					status="linkedInDomain";
				}
				
				bean.put("domainLink", domainLink);
				bean.put("link", url);
		
				
	// SeedUrl of LinkedIN			
				try 
				{
					bean.put("seedUrl", jsonObject.getString("homeUrl"));
				} catch (Exception e) {	bean.put("seedUrl", link);}
	
	// Address and Location			
				//StringUtils.stripAccents to convert latin alphabets to english.
				String location="",address="";
				try 
				{
					JSONObject addressObject = (JSONObject) jsonObject.get("headquarters");// gives
					String city,country,street1,state,zip;
					try 
					{
						city=StringUtils.stripAccents(addressObject.getString("city")).replaceAll("[-+.^:,|]","").trim();;			
					} 
					catch (Exception e) {	city="";	}
					
					try 
					{						
						country=StringUtils.stripAccents(addressObject.getString("country")).replaceAll("[-+.^:,|]","").trim();			
						
					} 
					catch (Exception e) {	country="";    }
					
					try 
					{						
						street1=StringUtils.stripAccents(addressObject.getString("street1")).replaceAll("[-+.^:,|]","").trim();			
						
					} 
					catch (Exception e) {	street1="";	}
					
					try 
					{
						state=StringUtils.stripAccents(addressObject.getString("state")).replaceAll("[-+.^:,|]","").trim();
					} 
					catch (Exception e) {	state="";	}
					
					try 
					{
						zip=StringUtils.stripAccents(addressObject.getString("zip")).replaceAll("[-+.^:,|]","").trim();
					} 
					catch (Exception e) {	zip="";		}
						
					//Merge individuals fields to reframe location and address.
					location =  city+ ((city.trim().equals(""))?"":",")+ country;
					address =  street1+ ((street1.trim().equals(""))?"":",") + city + ((city.trim().equals(""))?"":",")
							+ state + ((state.trim().equals(""))?"":",") + country + ((country.trim().equals(""))?"":",")
							+ zip;
					
					location=location.trim();
					address=address.trim();
					
					// To remove Last comma.
					address=address.endsWith(",")? address.substring(0, address.length()-1):address;
					location=location.endsWith(",")? location.substring(0, location.length()-1):location;
				} 
				catch (Exception e) 
				{	
					address="";
				    location="";		
				}
				bean.put("location", location);
				bean.put("address", address);
				
	// Company Type.			
				try 
				{
					bean.put("type", jsonObject.getString("companyType"));
				} 
				catch (Exception e) { bean.put("type", ""); }
	
	// Specialities			
				try 
				{   String specialties_str="";
					specialties = (JSONArray) jsonObject.get("specialties");
					for (int i = 0; i < specialties.length(); i++) 
					{
						specialties_str = specialties_str + specialties.getString(i) + ",";
					}
					// To remove Last comma.
					specialties_str=specialties_str.endsWith(",")? specialties_str.substring(0, specialties_str.length()-1):specialties_str;
					
					bean.put("specialist", specialties_str);
				} 
				catch (Exception e) {	bean.put("specialist", ""); }
				
	// Industry Type
				try 
				{
					bean.put("industry",jsonObject.getString("industry"));
				} catch (Exception e) {	bean.put("industry","");  }
				
	// Founded Year			
				try 
				{
					founded = jsonObject.getString("yearFounded");
				} catch (Exception e) {	founded = "0"; 	}
				
	// Company Size			
				try 
				{
					bean.put("companySize", jsonObject.getString("size"));
				} 
				catch (Exception e) {	bean.put("companySize", "");}
				
	// Default Vales to be added.			
				bean.put("systemDate",new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());
				bean.put("moduleName", "Sellers");
				bean.put("subCategory", "Sellers");
				bean.put("image", "no.jpg");
				bean.put("sellerStatus", status);
				bean.put("metaDescriptionStatus", "false");
				bean.put("contactInfoStatus", "false");
				bean.put("alexaStatus", "false");
				bean.put("imageStatus", "false");
				bean.put("sitetechStatus", "false");
				bean.put("status", "false");
				
	//Call DB Insertion method. pass bean Map and founded year.
				if(insertIntoDB(id,founded,bean))
				{
					// Console display after successful Insertion.			
					System.out.println("\n******************************\n"
							+"ID="+id+"\t Time="+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date()))
							+"\tFree Memory= "+(Runtime.getRuntime().freeMemory()/ (1024*1024))+" MB"
							+"\nLink="+url+"\n"
							+"Domainlink="+domainLink+"\n"
							+"Status= "+status);					
				}
				else
				{
					// Console display after Exception Occurred.			
					System.out.println("\n******************************\n"
							+"ID="+id+"\t Time="+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date()))
							+"\nLink="+url+"\n"
							+"Status= Exception Occurred.");
				}	
	
	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	// This method will add bean to BasicDBObject and some remaining fields. then it will insert into DB.
	public boolean insertIntoDB(String id,String founded,Map<String,String> bean)
	{		
		BasicDBObject insertObject = new BasicDBObject();
				
		try
		{	
			// Add all bean fields.
			insertObject.putAll(bean);
			
			//Call to get GeoLocation(lat and long)
			String[] geoData=updateLocation(bean.get("location"));
			
			// For geoip field in DB. Structure for ElasticSearch Query
			DBObject dbObject = (DBObject) JSON.parse(geoData[1]);	
				
			// Add remaining fields.
			insertObject.put("founded", Integer.parseInt(founded));
			insertObject.put("geoLocation", geoData[0]);
			insertObject.put("geoip",dbObject);
			insertObject.put("alexaRanking", Long.parseLong("0"));			
			
			// Insert companies 27 fields into parsed collection.
			parsedCollection.insert(insertObject);
			
			// Update Parsing status in htmlDocument collection.
			htmlDoc.update(new BasicDBObject().append("_id", Integer.parseInt(id)), new BasicDBObject().append("$set", new BasicDBObject().append("parsingStatus", "parsed")));
			
			return true;
		}
		catch(DuplicateKeyException e) // Update parsing status to duplicate.
		{
			System.err.println("Collection="+parsedCollection.getName()+"   Duplicate Key ID="+id);
			htmlDoc.update(new BasicDBObject().append("_id", Integer.parseInt(id)), new BasicDBObject().append("$set", new BasicDBObject().append("parsingStatus", "duplicate")));
			HtmlParserThreading.duplicate++;
			return false;
		}
		catch(Exception e) // May be WriteConcern Exception will come here.
		{
			System.err.println("Exception Occured for Updating ID="+id+"\t Exception"+e.getMessage());
			return false;
		}
	}	
	
	// This method will get geo-coordinates from geoLocation Collection for company location.
	public String[] updateLocation(String alllocation) 
	{
		String allLatlong = "",latlong="";
		String outputGeo[]=new String[2]; //return after method is finished.
		try
		{	
			// Get Unique set of location.
			Set<String> locationset = new LinkedHashSet<>();
			DBCursor cursor=null;
			DBObject present=null;			
			
			//Split location by comma and add to set
			String[] locations = alllocation.split(",");
			for (int i = 0; i < locations.length; i++) 
			{
				if(!locations[i].trim().equals(""))
				{
					locationset.add(locations[i].trim());
				}
			}
			
			// JSON String for geoip.
			String query = "'coordinates' : [ ";
				
			//If location is empty
			if(alllocation.trim().equals("") || alllocation.trim().equals(",") )
			{
				latlong="0.0,0.0";
				allLatlong = allLatlong + latlong + "/";
				query += "[ 0.0, 0.0 ]";
			}
			else // If location is present.
			{	
			  int count=0;
			  int i;
			  String lon[] ;
			  // Traverse location added to set
			  for (String location : locationset) 
			  {
				location=StringUtils.stripAccents(location);			
				location= location.replaceAll("[-+.^:,|]","").trim();
				cursor = geodata.find(new BasicDBObject("cityname", Pattern.compile("^" + location + "$", Pattern.CASE_INSENSITIVE)));	//If we don not have query then keep brackets blank. It will fetch all records
				cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);	
				
				if(cursor.count()==0) //If location not present in geoLocation DB.
				{
					latlong="0.0,0.0";
					allLatlong = allLatlong + latlong + "/";
					query += "[ 0.0, 0.0 ]";
				}
				else //If found.
				{			
					i=0;
					lon=null;
					 // Add geo coordinate of each location to JSON String
					while (cursor.hasNext()) //most of the time only one location is found. but still for safer-side i used while loop.
					{
						present=cursor.next();
						latlong=present.get("latlong").toString();
						
						//LatLong stored in DB is seperated by comma so split them.
						lon = latlong.split(",");
						
						if (i == cursor.size() - 1) 
						{
							query += "[" + lon[1] + "," + lon[0] + "]";							
						} 
						else
						{
							query += "[" + lon[1] + "," + lon[0] + "],";
							i++;
						}
						
						allLatlong = allLatlong + latlong + "/"; // String of Geo coordinates seperated by slash.
					}
				}
				
				if(count<locationset.size()-1)
				{
					query += ",";
				}
				count++;
				
			  }
			}
			query += "]";
			
			// Complete JSON String
			query="{" + "'type' : 'MultiPoint'," + query + "" + "}";
			
			outputGeo[0]=allLatlong; // String of Geo coordinates.
			outputGeo[1]=query;      // JSON String of Geo coordinates.

		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputGeo; // return array.
	}
}
		
		
	